#
# This file is part of pyperplan.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#

"""
Building the search node and associated methods
"""

import matplotlib.pyplot as plt
import networkx as nx
import pygraphviz
from networkx.drawing.nx_agraph import graphviz_layout, write_dot

from task import Operator

G = nx.MultiDiGraph()
root = None
node_id = 0
global_solution = []

class InfoAlg:
    search = ""
    heuristic = ""

class SearchNode:
    """
    The SearchNode class implements recursive data structure to build a
    search space for planning algorithms. Each node links to is parent
    node and contains informations about the state, action to arrive
    the node and the path length in the count of applied operators.
    """
    def __init__(self, state, parent, action, g):
        """
        Construct a search node

        @param state: The state to store in the search space.
        @param parent: The parent node in the search space.
        @param action: The action which produced the state.
        @param g: The path length of the node in the count of applied
                  operators.
        """
        self.state = state
        self.parent = parent
        self.action = action
        self.g = g

    def extract_solution(self):
        """
        Returns the list of actions that were applied from the initial node to
        the goal node.
        """
        solution = []
        global global_solution
        global_solution = []
        while self.parent is not None:
            global_solution.append(self.state)
            global_solution.append(self.action)
            solution.append(self.action)
            self = self.parent
        global_solution.append(self.state)
        global_solution.reverse()
        solution.reverse()


        return solution


def make_root_node(initial_state):
    """
    Construct an initial search node. The root node of the search space
    does not links to a parent node, does not contains an action and the
    g-value is zero.

    @param initial_state: The initial state of the search space.
    """
    global node_id
    G.add_node(initial_state, id = node_id)
    node_id += 1
    return SearchNode(initial_state, None, None, 0)


def make_child_node(parent_node, action, state):
    """
    Construct a new search node containing the state and the applied action.
    The node is linked to the given parent node.
    The g-value is set to the parents g-value + 1.
    """
    global node_id
    if not (state in G.nodes()):
        G.add_node(state, id = node_id)
        node_id += 1
    G.add_edge(parent_node.state, state, action = action)
    return SearchNode(state, parent_node, action, parent_node.g + 1)

def show():
    plt.figure(figsize=(8, 8))
    labels = {}
    nodes = []
    initNode = None

    path_nodes = []
    path_edges = []
    path_edges_actions = {}
    for index, object in enumerate(global_solution):
        if isinstance(object, frozenset):
            path_nodes.append(object)
        elif isinstance(object, Operator):
            node_from = global_solution[index - 1]
            for edge in G.out_edges_iter(nbunch=[node_from], data=True):
                if edge[1] == global_solution[index + 1] and edge[2]["action"] == object:
                    edge_tuple = (node_from, edge[1])
                    path_edges.append(edge_tuple)
                    path_edges_actions[edge_tuple] = edge[2]["action"]

    for node, data in G.nodes_iter(data=True):
        labels[node] = data['id']
        if data['id'] == 0:
            initNode = node
        else:
            if node not in path_nodes:
                nodes.append(node)

    pos = graphviz_layout(G, prog="neato", args='')

    path_nodes = path_nodes[1:]

    if initNode is not None:
        nx.draw(G, pos, edgelist=[],
                           nodelist=[initNode],
                           node_color="b",
                           node_size = 600,
                           alpha = 0.8)
    nx.draw(G, pos, edgelist=G.edges(),
                           nodelist = nodes,
                           node_color = "r",
                           node_size = 600,
                           alpha = 0.8)
    nx.draw_networkx_labels(G, pos, labels, ax=None, font_size = 12)
    nx.draw_networkx_nodes(G, pos,
                           nodelist=path_nodes,
                           node_size=700,
                           node_color="g",
                           alpha=0.8)
    nx.draw_networkx_edges(G, pos,
                           edgelist=path_edges,
                           width=5,
                           edge_color="g",
                           alpha=0.7)
    nx.draw_networkx_edge_labels(G, pos,
                           edge_labels = {edge: path_edges_actions[edge].name for edge in path_edges},
                           alpha = 0.5,
                           label_pos=0.5,
                           font_size=8)
    plt.axis('equal')
    filename = "visual_graphs/{}_{}".format(InfoAlg.search, InfoAlg.heuristic)
    write_dot(G, "{}.dot".format(filename))
    plt.savefig("{}.png".format(filename))
