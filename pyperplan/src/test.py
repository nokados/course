import subprocess
import time

class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            print ('elapsed time: %f ms' % self.msecs)

with Timer() as t:
	subprocess.call(["python", "-m", "memory_profiler", "pyperplan.py", "-l", "debug", 
		"-H", "hff",
		"-s", "gbf",
		"/home/nokados/Projects/course/pyperplan/benchmarks/airport/domain02.pddl", "/home/nokados/Projects/course/pyperplan/benchmarks/airport/task02.pddl"])

print (t.msecs)