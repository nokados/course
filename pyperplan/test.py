import subprocess
import time
import resource

programms = (
	("openstacks/domain05.pddl", "openstacks/task05.pddl"),
	("tpp/domain.pddl", "tpp/task06.pddl"),
	("sokoban/domain.pddl", "sokoban/task05.pddl"),
	("transport/domain.pddl", "transport/task06.pddl"),
	("woodworking/domain.pddl", "woodworking/task05.pddl")
	)
searches = ('ids', 'sat', 'astar', 'wastar', 'gbf', 'bfs', 'ehs')
heuristics = ('hff', 'lmcut', 'landmark', 'hadd', 'hsa', 'hmax', 'blind')
class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            print ('elapsed time: %f ms' % self.msecs)
for task in programms:
	for search in searches:
		for heuristic in heuristics:
			for experimentNumber in range(1, 51):
				results = open("data.csv", "a")
				results.write(task[0]+";"+task[1]+';'+search+';'+heuristic+';'+str(experimentNumber)+';')
				results.close()
				subprocess.call(["python3", "src/pyperplan.py", "-l", "error", 
					"-H", heuristic,
					"-s", search,
					"benchmarks/"+task[0], "benchmarks/"+task[1]])
			if (search in ['bfs', 'ids', 'sat']):
				break