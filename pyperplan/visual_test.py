import subprocess
import time
import resource

programms = (
	# ("blocks/domain.pddl", "blocks/task05.pddl"),
	#("logistics/domain.pddl", "logistics/task10.pddl"),
	#("movie/domain.pddl", "movie/task06.pddl"),
	#("pegsol/domain.pddl", "pegsol/task05.pddl"),\
	# ("tpp/domain.pddl", "tpp/task05.pddl"),
	("blocks/domain.pddl", "blocks/task15.pddl"),
	)
''''ids', 'sat',   'bfs',''' 
searches = ('astar', 'wastar', 'gbf',  'ehs')
heuristics = ('hff', 'lmcut', 'landmark', 'hadd', 'hsa', 'hmax', 'blind')


for task in programms:
	for search in searches:
		for heuristic in heuristics:
			if (search == 'ehs' and heuristic in {"hmax", 'blind', 'lmcut', 'landmark'}) or (search == 'astar' and heuristic in ['hmax',]):
				continue
			subprocess.call(["python3", "src/pyperplan.py", "-l", "error", 
				"-H", heuristic,
				"-s", search,
				"benchmarks/"+task[0], "benchmarks/"+task[1]])
			if (search in ['bfs', 'ids', 'sat']):
				break